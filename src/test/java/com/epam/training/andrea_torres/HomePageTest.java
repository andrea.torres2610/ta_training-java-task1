package com.epam.training.andrea_torres;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.jupiter.api.Assertions.*;

class HomePageTest {

    private WebDriver driver;
    private HomePage homePage;

    @BeforeEach
    public void start(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        homePage = new HomePage(driver);
    }

    @Test
    public void createNewPaste() {

        String code = "Hello from WebDriver";
        String expiration = "10 Minutes";
        String title = "helloweb";

        homePage.navigateHomePage();
        homePage.waitForPage();
        homePage.createNewPaste(code,expiration,title);

        NewPastePage newPastePage = new NewPastePage(driver);
        newPastePage.waitForPage();

        assertEquals(title, newPastePage.getPasteTitle(), "The title displayed is not matching");
        assertEquals("10 MIN", newPastePage.getPasteExpiration(), "The code displayed is not matching");
        assertEquals(code, newPastePage.getPasteCode(), "The code displayed is not matching");
    }

    @AfterEach
    void tearDown(){
        driver.quit();
    }
}