package com.epam.training.andrea_torres;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class NewPastePage {
    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//div[@class='info-top']/h1")
    private WebElement title;

    @FindBy(xpath = "//div[@class='expire']")
    private WebElement expiration;

    @FindBy(xpath = "//div[@class='source text']//div[@class='de1']")
    private WebElement code;

    public NewPastePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    public void waitForPage(){
        WebElement elementOnNewPage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='post-view js-post-view']")));
    }

    public String getPasteTitle(){
        return title.getText();
    }

    public String getPasteExpiration(){
        return expiration.getText();
    }

    public String getPasteCode(){
        return code.getText();
    }

}
