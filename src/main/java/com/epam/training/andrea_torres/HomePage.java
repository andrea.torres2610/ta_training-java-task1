package com.epam.training.andrea_torres;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomePage {

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(id = "postform-text")
    private WebElement codeBox;

    @FindBy(xpath = "//span[@aria-labelledby='select2-postform-expiration-container']")
    private WebElement expirationDropdown;

    @FindBy(xpath = "//li[text()='10 Minutes']")
    private WebElement expiration10MinOption;

    @FindBy(id = "postform-name")
    private WebElement titleBox;

    @FindBy(xpath = "//button[text()='Create New Paste']")
    private WebElement submitButton;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    public void navigateHomePage(){
        driver.get("https://pastebin.com/");
    }

    public void waitForPage(){
        WebElement homePageElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("postform-text")));
    }

    public void findOptionalPasteSettings(){
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(By.className("select2-selection__arrow"))).sendKeys(org.openqa.selenium.Keys.PAGE_DOWN).build().perform();
    }

    public void createNewPaste(String code, String expiration, String title){

        codeBox.sendKeys(code);

        findOptionalPasteSettings();

        expirationDropdown.click();

        wait.until(ExpectedConditions.elementToBeClickable(expiration10MinOption));

        expiration10MinOption.click();

        titleBox.sendKeys(title);

        submitButton.click();
    }
}